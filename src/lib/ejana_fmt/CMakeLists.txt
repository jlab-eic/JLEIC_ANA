# Build FMT library... if needed
message(STATUS "ejana: Building fmt library")
add_library(ejana_fmt SHARED "")

target_sources_local(ejana_fmt
        PRIVATE
        src/format.cc
        src/posix.cc)

target_include_directories(ejana_fmt PUBLIC ${CMAKE_CURRENT_LIST_DIR}/include )
set_target_properties(ejana_fmt PROPERTIES POSITION_INDEPENDENT_CODE ON)

set(FMT_LIB ejana_fmt)
set(FMT_INCLUDE ${CMAKE_CURRENT_LIST_DIR}/include)
install(TARGETS ejana_fmt LIBRARY DESTINATION lib)
install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/include DESTINATION .)