#ifndef EJANA_MINIMODEL_MC_FLUX_HIT_HH
#define EJANA_MINIMODEL_MC_FLUX_HIT_HH

#include <JANA/JObject.h>
#include <JANA/JFactory.h>
#include <ejana/EStringHelpers.h>

namespace minimodel
{
	class McFluxHit : public JObject
	{
	public:
		uint64_t id;          // Unique hit id
		uint64_t track_id;    // Reference to track
		uint64_t plane_id;    // Reference to plane
		double x;          // [mm]
		double y;          // [mm]
		double z;          // [mm]
		double e_loss;      // Energy deposition dE/dx [GeV]
		std::string vol_name; // Geometry volume name
	};
}
#endif  // EJANA_MINIMODEL_MC_FLUX_HIT_HH

