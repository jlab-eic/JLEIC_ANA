# Minimalistic Data Model

**TL;DR;** This dir contains pretty minimalistic data/class model for MC data.

Defining the data structure (in terms of class structure and its relations)
- is very important. And absolutely impossible at this moment (in 2019) for the EIC.
So classes in this directory don't even try to kill that dragon, instead, they are
dead simple POD objects serving more like tutorial and easy to understand role.

By doing analysis and 

Simplifications include:

- Data classes are POD-like 
    > they can't be really POD types in terms of C++11 as they are 
inherited from JObject. But they stay "Plain Old Data" in their nature and hearts

- Classes like TVector3 are not used inside this classes, so it is (x,y,z)
    > It might be changed in future

- There are limited interconnections between classes (like pointers to each other). 
    > It also may change in the future 




