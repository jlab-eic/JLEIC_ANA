
#include <TVector3.h>

#ifndef _EIC_PROTO_GENERATOR_
#define _EIC_PROTO_GENERATOR_

#include <FairGenerator.h>

class EicProtoGenerator: public FairGenerator
{
 public:
 EicProtoGenerator(const char* name): FairGenerator(name), mVerticalBeamDivergence(0.0), 
    mHorizontalBeamDivergence(0.0), mHorizontalBeamRotation(0.0) {};
  ~EicProtoGenerator() {};

  // This stuff is only good for rough forward proton modeling estimates; 
  // a random 3D rotation with these parameters in [rad] will be applied to the 
  // outgoing particles;
  void SetNaiveVerticalBeamDivergence  (double value) { mVerticalBeamDivergence   = value; };
  void SetNaiveHorizontalBeamDivergence(double value) { mHorizontalBeamDivergence = value; };

  void SetNaiveHorizontalBeamRotation  (double value) { mHorizontalBeamRotation   = value; };

  TVector3 GetModifiedTrack(const TVector3 track);

  double mVerticalBeamDivergence, mHorizontalBeamDivergence, mHorizontalBeamRotation;

  ClassDef(EicProtoGenerator,1);
};

#endif
