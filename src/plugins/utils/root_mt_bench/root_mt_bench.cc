#include "MultiThreadingAnalysis.h"
#include "DummyEventSource.h"


// Routine used to create our JEventProcessor
#include <JANA/JApplication.h>
#include <JANA/JFactoryGenerator.h>
#include <JANA/JEventSourceGeneratorT.h>

extern "C" {
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new JEventSourceGeneratorT<DummyEventSource>(app));
        app->Add(new MultiThreadingAnalysis(app));
        app->Add(new JFactoryGeneratorT<JFactoryT<DummyEventDataOther>>());
    }
}