//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Feb 22 02:19:03 2019 by ROOT version 6.16/00
// And updated later many times manually (he-he)
//////////////////////////////////////////////////////////

#ifndef JLEICGeantSelector_3_0_h
#define JLEICGeantSelector_3_0_h

#include "RootEventSelector_1_0.h"

using std::string;

class JLEICGeantSelector_3_0 : public JLEICGeantSelector_2_0 {
public :
    explicit JLEICGeantSelector_3_0(TTree *tree): JLEICGeantSelector_2_0(tree)
    {}

    TTreeReaderValue<ULong64_t>    ce_emcal_count = {fReader, "ce_emcal_count"};
    TTreeReaderArray<std::string>  ce_emcal_name  = {fReader, "ce_emcal_name" };
    TTreeReaderArray<int>  ce_emcal_id       = {fReader, "ce_emcal_id"      };
    TTreeReaderArray<int>  ce_emcal_row      = {fReader, "ce_emcal_row"     };
    TTreeReaderArray<int>  ce_emcal_col      = {fReader, "ce_emcal_col"     };
    TTreeReaderArray<int>  ce_emcal_section  = {fReader, "ce_emcal_section" };

    TTreeReaderArray<double>  ce_emcal_etot_dep = {fReader, "ce_emcal_etot_dep"};
    TTreeReaderArray<int>     ce_emcal_npe      = {fReader, "ce_emcal_npe"     };
    TTreeReaderArray<double>  ce_emcal_adc      = {fReader, "ce_emcal_adc"     };
    TTreeReaderArray<double>  ce_emcal_tdc      = {fReader, "ce_emcal_tdc"     };
    TTreeReaderArray<double>  ce_emcal_xcrs     = {fReader, "ce_emcal_xcrs"    };
    TTreeReaderArray<double>  ce_emcal_ycrs     = {fReader, "ce_emcal_ycrs"    };
    TTreeReaderArray<double>  ce_emcal_zcrs     = {fReader, "ce_emcal_zcrs"    };

    TTreeReaderArray<unsigned long> hit_trk_index         = {fReader, "hit_trk_index"};
    TTreeReaderArray<long>          hit_pdg               = {fReader, "hit_pdg"};
    TTreeReaderArray<unsigned long> hit_type              = {fReader, "hit_type"};
    TTreeReaderArray<unsigned long> trk_parent_index      = {fReader, "trk_parent_index"};
    TTreeReaderArray<unsigned long> gen_prt_vtx_index     = {fReader, "gen_prt_vtx_index"};
    TTreeReaderArray<unsigned long> gen_prt_trk_index     = {fReader, "gen_prt_trk_index"};
};

#endif