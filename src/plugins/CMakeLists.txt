add_subdirectory(analysis)
add_subdirectory(examples)
add_subdirectory(io)
add_subdirectory(reco)
add_subdirectory(utils)

include_directories(
        analysis
        examples
        io
        reco
        utils
)