# Vector meson analysis

### CLion arguments

```
-Pplugins=beagle_reader,vmeson
-Pvmeson:verbose=0
-Pvmeson:e_beam_energy=5
-Pvmeson:ion_beam_energy=50
-Pjana:DEBUG_PLUGIN_LOADING=1
-Pnevents=100000
-Pnthreads=1
-CPROOT:StartGui=1
/home/romanov/eic/data/2019-04-15-beagle_mc_samples/eD_5x50_Q2_1_10_y_0.01_0.95_tau_7_noquench_kt=ptfrag=0.32_Shd1_ShdFac=1.32_Jpsidifflept_test40k_fixpf.txt
```

Working directory:
```
$PROJECT_DIR$/work
```

Environment variables:
```
JANA_PLUGIN_PATH=$PROJECT_DIR$/cmake-build-debug
```



