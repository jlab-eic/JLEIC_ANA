
#include <JANA/JEventProcessor.h>
#include <JANA/Services/JGlobalRootLock.h>

#include <ejana/EServicePool.h>

#include <TDirectory.h>
#include <TH1D.h>

#include <io/g4e_reader/GeantPrimaryParticle.h>
#include <TH2D.h>

class JApplication;

class TrackingEfficiencyProcessor : public JEventProcessor {
private:

    std::string m_reco_track_input = "genfit";
    // "acts"          => Use ACTS (incl vertex finding) from RecoTrackFactory_Acts
    // "genfit"        => Use Genfit (no vertexing) from RecoTrackFactory_Genfit
    // "truths"        => Use MC track values from RecoTrackFactory_MC


    ej::EServicePool services_;  // TODO: Replace with JServiceLocator
    int verbose_ = 0;  // Output verbosity
    std::shared_ptr<JGlobalRootLock> m_lock;
    TDirectory *plugin_root_dir;   // Main TDirectory for Plugin histograms and data

    TH1D *h1d_pt_primaryparticle;
    TH1D *h1d_pt_true_all;
    TH1D *h1d_pt_reco_true;
    TH2D *h2d_dptptp_reco;
    TH1D *h1d_pt_reco;
    TH1D *h1d_pt_diff;
    TH1D *h1d_tracking_efficiency;
    TH1D *h1d_tracking_misfits;
    TH1D *h1d_x_true;
    TH1D *h1d_y_true;
    TH1D *h1d_z_true;
    TH1D *h1d_x_reco;
    TH1D *h1d_y_reco;
    TH1D *h1d_z_reco;

    std::set<int> pdgs_;  // All pdgs encountered so far

public:
    explicit TrackingEfficiencyProcessor(JApplication *app = nullptr) :
            JEventProcessor(app),
            services_(app) {};

    void Init() override;
    void Process(const std::shared_ptr<const JEvent> &event) override;
    void Finish() override;

};

