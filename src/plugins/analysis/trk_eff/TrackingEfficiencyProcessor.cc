#include "TRandom.h"
#include "TLorentzVector.h"
#include <TFile.h>

#include <fmt/core.h>

#include "TrackingEfficiencyProcessor.h"

#include <MinimalistModel/McTrack.h>
#include <RecoModel/RecoVertex.h>
#include <io/g4e_reader/GeantPrimaryVertex.h>
#include <MinimalistModel/McGeneratedVertex.h>
#include "RecoModel/RecoHit.h"
#include "RecoModel/RecoTrackCand.h"
#include "RecoModel/RecoTrack.h"

using namespace fmt;

void TrackingEfficiencyProcessor::Init()
{
    auto app = GetApplication();
    m_lock = app->GetService<JGlobalRootLock>();
    auto pm = app->GetService<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    verbose_ = 0;
    pm->SetDefaultParameter("trk_eff:verbose", verbose_, "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    pm->SetDefaultParameter("trk_eff:fitter", m_reco_track_input, "Track fitting algorithm. Options are 'genfit' or 'acts'");

    // Setup histograms. We get current TFile from Service Locator
    auto file = services_.Get<TFile>();


    m_lock->acquire_write_lock();

    // create a subdirectory "hist_dir" in this file
    plugin_root_dir = file->mkdir("trk_eff");
    plugin_root_dir->cd();         // Just in case some other root file is the main TDirectory now

    h1d_pt_primaryparticle = new TH1D("pt_primary_particle", "tranverse p", 100,0,15);
    h1d_pt_true_all = new TH1D("pt_true_all", "All tracks pt values (with or without reco)", 100,0,15);

    h1d_pt_reco = new TH1D("pt_reco", "Reconstructed Pt", 100,0,15);
    h1d_pt_reco_true = new TH1D("pt_reco_true", "True values of reconstructed Pt", 100, 0, 15);

    h1d_pt_diff = new TH1D("pt_diff", "diff p", 100,-1,1);

    h1d_tracking_efficiency = new TH1D("tracking_efficiency", "successful fits/reconstructible fits", 100, 0, 1);

    h1d_tracking_misfits = new TH1D("tracking_misfits", "failed fits/reconstructible fits", 100, 0, 1);

    h1d_x_true = new TH1D("x_true", "True vertex x", 100, -10, 10);
    h1d_y_true = new TH1D("y_true", "True vertex y", 100, -10, 10);
    h1d_z_true = new TH1D("z_true", "True vertex y", 100, -10, 10);
    h1d_x_reco = new TH1D("x_reco", "Reconstructed vertex x", 100, -10, 10);
    h1d_y_reco = new TH1D("y_reco", "Reconstructed vertex y", 100, -10, 10);
    h1d_z_reco = new TH1D("z_reco", "Reconstructed vertex z", 100, -10, 10);
    h2d_dptptp_reco = new TH2D("dptpt", "dpt/pt of pt", 200, 0, 15, 200, 0, 0.1);

    // TODO: Do we want these?
    /*hD0_mass = new TH1D("D0_mass", "D0 mass", 100,1.45,2.2);
    hD0_mass->GetXaxis()->SetTitle("m_{K#pi} [GeV/c^{2}]");
    hD0_mass->SetDirectory(plugin_root_dir);

    hD0_vtx= new TH1D("D0_vtx", "D0 vtx", 100,0.,300.);
    hD0_vtx->GetXaxis()->SetTitle("vertex [#mum]");
    hD0_vtx->SetDirectory(plugin_root_dir);

    hD0_pt = new TH1D("D0_pt", "D0 pt", 100,0.,5.);
    hD0_pt->GetXaxis()->SetTitle("p_{T} [GeV]");
    hD0_pt->SetDirectory(plugin_root_dir);*/

    m_lock->release_lock();
}


void TrackingEfficiencyProcessor::Process(const std::shared_ptr<const JEvent> &event)
{

    print("\n==================================== {} \n", event->GetEventNumber());
    auto hits = event->Get<RecoHit>();
    auto clusters = event->Get<RecoTrackCand>();
    auto mc_tracks = event->Get<minimodel::McTrack>();
    auto mc_generated_vertices = event->Get<minimodel::McGeneratedVertex>();
    auto geant_primary_vertices = event->Get<jleic::GeantPrimaryVertex>();
    auto geant_primary_particles = event->Get<jleic::GeantPrimaryParticle>(); // TODO: What if these don't exist?

    auto reco_tracks = event->Get<RecoTrack>(m_reco_track_input);
    auto track_metadata = event->GetMetadata<RecoTrack>(m_reco_track_input);

    m_lock->acquire_write_lock();

    if (verbose_) {
        // We want all of this inside the lock so that all per-event data shows up together
        print("trk_eff::Process( event number = {} ) \n", event->GetEventNumber());

        for (auto hit : hits) {
            //print("RecoHit track {}: {}, {}, {}\n", hit->track_id, hit->Xpos, hit->Ypos, hit->Zpos);
        }
        /*for (auto vertex: mc_generated_vertices) {
            print("MC track  {}, x,y,z = {}, {}, {} \n", vertex->id, vertex->x, vertex->y, vertex->z);
        }*/
        for (auto track : mc_tracks) {
            auto mc_pt = track->p * sqrt(track->dir_x * track->dir_x + track->dir_y * track->dir_y);

        }
        for (auto track : reco_tracks) {

            print("Reco track {:<10} Pxyz={:<10} {:<10} {:<10} pt={:<10} p={:<10} chi={:<10} ndf={}\n", track->track_id, track->p.Px(), track->p.Py(), track->p.Pz(), track->p.Pt(), track->p.Mag(), track->chisq, track->ndf);
        }
    }

    // Process raw Geant4 events if available
    uint64_t primary_particle_count = 0;
    for(auto particle: geant_primary_particles) {
        pdgs_.insert(particle->pdg);
        double pt_true = particle->tot_mom*sqrt(particle->dir_x*particle->dir_x + particle->dir_y * particle->dir_y);
        h1d_pt_primaryparticle->Fill(pt_true);
        primary_particle_count++;
    }

    std::map<uint64_t, const minimodel::McTrack*> track_lookup;
    for (auto mc_track : mc_tracks) {
        assert(track_lookup.find(mc_track->id) == track_lookup.end()); // Tracks must have unique ID
        track_lookup[mc_track->id] = mc_track;

        auto mc_pt = mc_track->p * sqrt(mc_track->dir_x * mc_track->dir_x + mc_track->dir_y * mc_track->dir_y);
        h1d_pt_true_all->Fill(mc_pt);
    }

    for(auto reco_track : reco_tracks) {

        auto mc_track = track_lookup[reco_track->track_id];
        if (mc_track != nullptr) {
            auto mc_pt = mc_track->p * sqrt(mc_track->dir_x * mc_track->dir_x + mc_track->dir_y * mc_track->dir_y);
            double dpt = reco_track->p.Pt() - mc_pt;

            if(verbose_) {
                print("Trk id={:<4} dPt={:<10} dP={:<10}\n", mc_track->id,  std::abs(dpt), std::abs(reco_track->p.Mag()-mc_track->p));
                print("  MC Pxyz=[{:<10} {:<10} {:<10}] pt={:<10} p={:<10}\n", mc_track->px, mc_track->py, mc_track->pz, mc_pt, mc_track->p);
                print("  RE Pxyz=[{:<10} {:<10} {:<10}] pt={:<10} p={:<10}\n", reco_track->p.Px(), reco_track->p.Py(), reco_track->p.Pz(), reco_track->p.Pt(), reco_track->p.Mag());
            }


            h1d_pt_reco->Fill(reco_track->p.Pt());
            h1d_pt_reco_true->Fill(mc_pt);

            h1d_pt_diff->Fill(dpt);
            h2d_dptptp_reco->Fill(mc_pt, std::abs(dpt/mc_pt));

            h1d_x_true->Fill(mc_track->vtx_x);
            h1d_y_true->Fill(mc_track->vtx_y);
            h1d_z_true->Fill(mc_track->vtx_z);
        }

        h1d_x_reco->Fill(reco_track->x.x());
        h1d_y_reco->Fill(reco_track->x.y());
        h1d_z_reco->Fill(reco_track->x.z());
    }

    double eff = (1.0 * track_metadata.succeeded_count) /
                 (track_metadata.total_track_count - track_metadata.insufficient_data_count);

    h1d_tracking_efficiency->Fill(eff);

    double exc = (1.0 * track_metadata.misfit_count) /
                 (track_metadata.total_track_count - track_metadata.insufficient_data_count);

    h1d_tracking_misfits->Fill(exc);

    m_lock->release_lock();
}


void TrackingEfficiencyProcessor::Finish()
{
    fmt::print("Found pdgs: \n");
    for (int pdg: pdgs_) {
        fmt::print("{}, ", pdg);
    }
    fmt::print("\n");
}

