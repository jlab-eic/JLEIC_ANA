#include "Math/Vector4D.h"
#include "TVector3.h"

#include <JANA/JEvent.h>

#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <io/beagle_reader/BeagleEventData.h>

#include <dis/functions.h>
#include <ejana/EServicePool.h>
#include <fmt/format.h>     // For format and print functions
#include <fmt/core.h>
#include <MinimalistModel/DisInfo.h>

#include "DisPlotsProcessor.h"


// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
Double_t MassPI = 0.139570,
         MassK = 0.493677,
         MassP = 0.93827,
         MassE = 0.000511,
         MassMU = 0.105658;
//==================================================================


using namespace std;
using namespace fmt;

void DisPlotsProcessor::Init() {
    ///  Called once at program start.
    print("DisPlotsProcessor::Init()\n");

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    root_out.init(services.Get<TFile>());

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("dis:verbose", verbose,
                            "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    smearing = 0;
    pm->SetDefaultParameter("dis:smearing", smearing,
                            "Particle smearing 0-true MC, 1-smearing, 2-reconstruction");
    ncdis = 1;  // if charged current ncdis==0 ,   if neutral current ncdis=1;
    pm->SetDefaultParameter("diss:ncdis", ncdis, "NC DIS  1, CC DIS 0 ");
    // Beams energies
    pm->SetDefaultParameter("dis:e_beam_energy", e_beam_energy, "Energy of colliding electron beam");
    pm->SetDefaultParameter("dis:ion_beam_energy", ion_beam_energy, "Energy of colliding ion beam");

    if (verbose) {
        print("Parameters:\n");
        print("  dis_plots:verbose:         {0}{0}\n", verbose);
        print("  dis_plots:e_beam_energy:   {}\n", e_beam_energy);
        print("  dis_plots:ion_beam_energy: {}\n", ion_beam_energy);
        print("  dis_plots:ncdis: {}\n", ncdis);

    }
}


void DisPlotsProcessor::Process(const std::shared_ptr<const JEvent> &event) {
    ///< Called every event.
    using namespace fmt;
    using namespace std;
    using namespace minimodel;
    using namespace dis;

    std::lock_guard<std::recursive_mutex> locker(root_out.lock);

    double Xtrue, Ytrue, Q2true;
    double X_4v, Y_4v, Q2_4v;
    double X_em, Y_em, Q2_em;


    // >oO debug printing
    if (event->GetEventNumber() % 1000 == 0 && verbose > 1) {
        print("\n--------- EVENT {} --------- \n", event->GetEventNumber());
    }

    root_out.h1_event_num->Fill(event->GetEventNumber());
    // Get the inputs needed for this factory.

    // Get the inputs needed for this factory.
    auto gen_parts = event->GetFactory<McGeneratedParticle>("smear") ?   // If there is a smearing factory
                     event->Get<McGeneratedParticle>("smear") :
                     event->Get<McGeneratedParticle>();

    if(event->GetFactory<minimodel::DisInfo>()) {
        auto dis_info = event->GetSingle<minimodel::DisInfo>();
        Xtrue =  dis_info->x;
        Ytrue =  dis_info->y;
        Q2true = dis_info->q2;
    }

    if(event->GetFactory<ej::BeagleEventData>()) {
        auto beagle_event = event->GetSingle<ej::BeagleEventData>();
        Xtrue = beagle_event->truex;
        Ytrue = beagle_event->truey;
        Q2true = beagle_event->trueQ2;
    }

    root_out.h1_particle_num->Fill(gen_parts.size());

    //   --- calculate kinematic variables using electron only method -------------
    for (auto electron: gen_parts) {
        if ((abs(electron->pdg) == 11 && electron->charge < 0  && ncdis == 1) ||
            (abs(electron->pdg) == 12 && ncdis == 0)) {

            ROOT::Math::PxPyPzMVector p;
            p.SetPxPyPzE(electron->px, electron->py, electron->pz, electron->tot_e);

            TVector3 vertex;
            vertex.SetXYZ(electron->vtx_x * 1000., electron->vtx_y * 1000., electron->vtx_z * 1000.);

            if (verbose >= 2) { print("This is a place to check kinematics X,Q2 \n"); }

            double E_el = p.E();
            double Theta_el = p.Theta();
            double Empz_el = p.E() - p.Pz();
            //-------- here we need to feel True information 
            root_out.h2_XQ2_true->Fill(Xtrue, Q2true);
            root_out.h2_XQ2_true_log->Fill(log10(Xtrue), log10(Q2true));
            root_out.h1_x_true->Fill(Xtrue);
            root_out.h1_Q2_true->Fill(Q2true);
            root_out.h1_y_true->Fill(Ytrue);

            //--------- 4Vector method ------------------------
            double PISL[4] = {0., 0., -e_beam_energy, sqrt(e_beam_energy * e_beam_energy + MassE * MassE)};
            double PISP[4] = {0., 0., ion_beam_energy, sqrt(ion_beam_energy * ion_beam_energy + MassP * MassP)};

            double PFSL[4] = {p.px(), p.py(), p.pz(), sqrt(p.E() * p.E() + MassE * MassE)};

            GEN_XYQ2(PFSL, PISL, PISP, X_4v, Y_4v, Q2_4v);

            //        root_out.h2_XQ2_true->Fill(log10(Xtrue), log10(Q2true));
            root_out.h2_XQ2_4v->Fill(X_4v, Q2_4v);
            root_out.h2_XQ2_4v_log->Fill(log10(X_4v), log10(Q2_4v));
            root_out.h1_X_4v->Fill(X_4v);
            root_out.h1_Q2_4v->Fill(Q2_4v);
            root_out.h1_Y_4v->Fill(Y_4v);


            //--------Electron method ------------------------
            GEN_El_XYQ2(E_el, Theta_el, e_beam_energy, ion_beam_energy, X_em, Y_em, Q2_em);
            root_out.h2_XQ2_em_log->Fill(log10(X_em), log10(Q2_em));
            root_out.h2_XQ2_em->Fill(X_em, Q2_em);
            root_out.h1_x_em->Fill(X_em);
            root_out.h1_Q2_em->Fill(Q2_em);
            root_out.h1_y_em->Fill(Y_em);
            root_out.h1_empz->Fill(Empz_el);


            //-------------- check resolution ------------------------

            if (verbose >= 2) {
                print("DisPlots:Ee {:<11} Th {:<11}  (Xtrue {:<11} X_em {:<11}) ( Ytrue {:<11}  Y_em {:<11}) Q2true {:<11} Q2_em {:<11} \n",
                      p.E(), p.Theta(), Xtrue, X_em, Ytrue, Y_em, Q2true, Q2_em);
            }

            root_out.h2_Xem_diff->Fill(log10(Xtrue), (X_em - Xtrue) / Xtrue);
            root_out.h2_Yem_diff->Fill(log10(Ytrue), (Y_em - Ytrue) / Ytrue);
            root_out.h2_Q2em_diff->Fill(log10(Q2true), (Q2_em - Q2true) / Q2true);
            root_out.h1_q2_diff_em->Fill((Q2_em - Q2true) / Q2true);
            root_out.h1_x_diff_em->Fill((X_em - Xtrue) / Xtrue);
            root_out.h1_y_diff_em->Fill((Y_em - Ytrue) / Ytrue);

            root_out.h2_Xem_diff->Fill(log10(Xtrue), (X_em - Xtrue) / Xtrue);
            root_out.h2_Yem_diff->Fill(log10(Ytrue), (Y_em - Ytrue) / Ytrue);
            root_out.h2_Q2em_diff->Fill(log10(Q2true), (Q2_em - Q2true) / Q2true);


            //        root_out.h2_Q2_em_true->Fill(log10(Q2true), log10(Q2_em));

            //------------- apply cuts --------------------------
            if (Y_em > 0.05 && Y_em < 0.95 && Q2_em > 10) {
                root_out.h2_XQ2_em_cuts->Fill(log10(X_em), log10(Q2_em));
            }

            if (Y_4v > 0.05 && Y_4v < 0.95 && Q2_4v > 10) {
                root_out.h2_XQ2_4v_cuts->Fill(log10(X_4v), log10(Q2_4v));
            }
        }
    }
}


void DisPlotsProcessor::Finish() {
    ///< Called after last event of last event source has been processed.
    print("DisPlotsProcessor::Finish(). Cleanup\n");
}
