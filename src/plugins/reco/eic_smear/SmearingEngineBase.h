#ifndef EJANA_SEMARING_DETECTOR_BASE_H
#define EJANA_SEMARING_DETECTOR_BASE_H

#include <JANA/JObject.h>
#include <MinimalistModel/McGeneratedParticle.h>

namespace ej {
namespace smear {

    enum class SmearingResults {
        NotGoneThrough,     // particle hasn't gone through smearing
        Smeared
    };

    class SmearingEngineBase: public JObject {
    public:
        SmearingEngineBase() = default;

        explicit SmearingEngineBase(const std::string& name, int verbose=0):
            _verbose(verbose),
            _name(name)
        {}

        virtual SmearingResults Smear(minimodel::McGeneratedParticle &particle)=0;

    private:
        int _verbose;        // verbosity, 0 - none, 1 - some, 2 - all
        std::string _name;   // Semaring
    };

}}  // namespace close


#endif //EJANA_SEMARING_DETECTOR_BASE_H
