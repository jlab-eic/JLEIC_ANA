
# Linking eic-smear

# Build FMT library... if needed
message(STATUS "ejana: Building eicroot4jana library")

#add_subdirectory(${PROJECT_SOURCE_DIR}/src/external/eicroot4jana lib)

# CREATING THE PLUGIN
# ----------------------------------------------------------------------------------------------------------------------

# Automatically set plugin name the same as the direcotry name
# Don't forget string(REPLACE " " "_" PLUGIN_NAME ${PLUGIN_NAME}) if this dir has spaces in its name
get_filename_component(PLUGIN_NAME ${CMAKE_CURRENT_LIST_DIR} NAME)


print_header(">>>>   P L U G I N :   ${PLUGIN_NAME}    <<<<")       # Fancy printing

add_plugin(${PLUGIN_NAME})                                          # Creates plugin 'target'

# (!) GLOB is not recommended by CMake. So it is better to use:
# target_sources_local(${PLUGIN_NAME} PRIVATE your_file.cc ... PUBLIC your_file.h ...)
# But... GLOB here makes this file just hot pluggable
file(GLOB SRC_FILES *.cc *.cxx)
file(GLOB HEADER_FILES *.h)

# To somehow control GLOB lets at least name the files we are going to compile:
#message(STATUS "Source files to build:")    # Print source files to build
print_file_names("" "Source files to build:" ${SRC_FILES})

message(STATUS "Header files to build:")
print_file_names("" ${HEADER_FILES})            # Print header files

find_path(EIC_SMEAR_INCLUDE_DIR eicsmear/smear/Smear.h
        HINTS
        ${EIC_SMEAR_DIR}/include
        $ENV{EIC_SMEAR_HOME}/include
        )


find_library(EIC_SMEAR_LIBRARY
        NAMES eicsmear
        HINTS
        $ENV{EIC_SMEAR_HOME}
        ${EIC_SMEAR_DIR}
        PATH_SUFFIXES lib lib64
        )

message(STATUS "EIC_SMEAR_INCLUDE_DIR=${EIC_SMEAR_INCLUDE_DIR}")
message(STATUS "EIC_SMEAR_LIBRARY=${EIC_SMEAR_LIBRARY}")

# Add sources to our target
target_sources(${PLUGIN_NAME} PRIVATE ${SRC_FILES} PUBLIC ${HEADER_FILES})

target_include_directories(${PLUGIN_NAME} PUBLIC ${EIC_SMEAR_INCLUDE_DIR})

# Libraries used for this plugin
target_link_libraries(${PLUGIN_NAME} ${ROOT_LIBRARIES} -L${JANA_LIB_DIR} ${JANA_LIB} ejana_fmt ${EIC_SMEAR_LIBRARY} ${IMPORT_LIB})

install(DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/" # source directory
        DESTINATION "include/ejana/plugins/reco/${PLUGIN_NAME}" # target directory
        FILES_MATCHING PATTERN "*.h" # select header files
        )