#include "SmearingFactory.h"

// TODO organize includes
#include <JANA/JEventProcessor.h>
#include <JANA/JEvent.h>
#include "eicsmear/smear/Detector.h"
#include <ejana/EServicePool.h>
#include <MinimalistModel/McGeneratedParticle.h>

#include <fmt/format.h>     // For format and print functions
#include <cmath>
#include <mutex>
#include <memory>

#include "SmearDetectorFactory.h"
#include "EicSmearEngine.h"
#include "SimpleSmearEngine.h"


Smear::Detector BuildBeAST_0_0();
Smear::Detector BuildBeAST_0_1();
Smear::Detector SmearePHENIX_0_0(bool multipleScattering);
Smear::Detector BuildeStar_0_0();
Smear::Detector BuildHandBook_1_2();
Smear::Detector BuildPerfectDetector();
Smear::Detector BuildSTAR_0_0();
Smear::Detector BuildZEUS_0_0();
Smear::Detector BuildMatrixDetector_0_1();

void SmearParticleMatrix_v1_0_0(minimodel::McGeneratedParticle *particle, int _verbose);
void SmearPrticleJleic_v1_0_2(minimodel::McGeneratedParticle *particle, int _verbose);
void SmearPrticleJleic_v1_0_1(minimodel::McGeneratedParticle *particle, int _verbose);

void SmearingFactory::Init() {
    using SmearingEngineSharedPtr = std::shared_ptr<ej::smear::SmearingEngineBase>;
    static std::recursive_mutex lock;

    // 'locker' needs here as Build... functions tend to fall in DeadLock because of TFormula Streamer
    std::lock_guard<std::recursive_mutex> locker(lock);

    _detectors["matrix"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildMatrixDetector_0_1()));
    _detectors["matrix-0.1"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildMatrixDetector_0_1()));
    _detectors["beast"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildBeAST_0_1()));
    _detectors["beast-0.1"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildBeAST_0_1()));
    _detectors["beast-0.0"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildBeAST_0_0()));
    _detectors["ephenix"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(SmearePHENIX_0_0(true)));
    _detectors["ephenix_nms"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(SmearePHENIX_0_0(false)));
    _detectors["estar"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildeStar_0_0()));
    _detectors["star"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildSTAR_0_0()));
    _detectors["perfect"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildPerfectDetector()));
    _detectors["handbook"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildHandBook_1_2()));
    _detectors["handbook-1.2"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildHandBook_1_2()));
    _detectors["zeus"] = SmearingEngineSharedPtr(new ej::smear::EicSmearEngine(BuildZEUS_0_0()));
    _detectors["smatrix"] = SmearingEngineSharedPtr(new ej::smear::SimpleSmearEngine(SmearParticleMatrix_v1_0_0, _verbose));
    _detectors["jleic"] = SmearingEngineSharedPtr(new ej::smear::SimpleSmearEngine(SmearPrticleJleic_v1_0_2, _verbose));
    _detectors["jleic-0.2"] = SmearingEngineSharedPtr(new ej::smear::SimpleSmearEngine(SmearPrticleJleic_v1_0_2, _verbose));
    _detectors["jleic-0.1"] = SmearingEngineSharedPtr(new ej::smear::SimpleSmearEngine(SmearPrticleJleic_v1_0_1, _verbose));

    _detector_name="matrix";
    _verbose=0;

    auto pm = this->GetApplication()->GetJParameterManager();

    //jleic_eic_smear_detector = BuildJLEIC();
    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    pm->SetDefaultParameter("eic_smear:verbose", _verbose, "Plugin output level. 0-almost nothing, 1-some, 2-everything");
    pm->SetDefaultParameter("eic_smear:detector", _detector_name, "Detector name: handbook, beast, jleic, zeus");
    pm->SetDefaultParameter("eic_smear:throw", _throw_not_smeared, "Throw away not smeared particles");
    pm->SetDefaultParameter("eic_smear:strict", _strict_particle_selection,
                             "False - leave particles if at least anything is smeared. True - leave only particles with both e and p smeared");

    if(_detectors.count(_detector_name)) {
        _selected_engine = _detectors[_detector_name];
    }

/*
 *
    Each particle has smearing info with flags
    has_e;           /// Energy was smeared
    has_p;           /// Momentum was smeared
    has_pid;         /// PID is smeared
    has_vtx;         /// Vertex is smeared
    has_any_eppid;   /// Has smeared e, p, or pid
    There are two eic_smear plugin flags
    eic_smear:throw - Throw away not smeared particles
    eic_smear:strict - False - leave particles if at least anything is smeared. True - leave only particles with both e and p smeared

    .plugin('jana', output='beagle.root', nthreads=10)\
    .source('../data/beagle_eD.txt')
  */
}


void SmearingFactory::Process(const std::shared_ptr<const JEvent> &event) {
    using namespace fmt;
    using namespace std;

    // Get the inputs needed for this factory.
    auto source_particles = event->Get<minimodel::McGeneratedParticle>();

    vector<minimodel::McGeneratedParticle*> dest_particles;

    for (auto source_particle : source_particles) {
        if (!source_particle->is_stable) continue;

        std::unique_ptr<minimodel::McGeneratedParticle> dest_particle(new minimodel::McGeneratedParticle());
        *dest_particle = *source_particle;      // Copy all fields of source particle to dest particle

        dest_particle->has_smear_info = true;   // Indicate this particle gone through smearing
        // this particle may vanish during this function =):

        dest_particle->smear.orig_tot_e = dest_particle->tot_e;   /// original Energy
        dest_particle->smear.orig_p = dest_particle->p;           /// original total momentum
        dest_particle->smear.orig_px = dest_particle->px;         /// original px
        dest_particle->smear.orig_py = dest_particle->py;         /// original py
        dest_particle->smear.orig_pz = dest_particle->pz;         /// original pz
        dest_particle->smear.orig_vtx_x = dest_particle->vtx_x;   /// original px
        dest_particle->smear.orig_vtx_y = dest_particle->vtx_y;   /// original py
        dest_particle->smear.orig_vtx_z = dest_particle->vtx_z;   /// original pz

        auto & stat = GetStatistics();

        stat.total_particles++;

        ej::smear::SmearingResults result;

        if (auto engine = _selected_engine.lock()) {
            auto &dp = *dest_particle;
            result = engine->Smear(dp);
        } else {
            auto *jobj_detector = const_cast<ej::smear::SmearingEngineBase *>(event->GetSingle<ej::smear::SmearingEngineBase>(_detector_name));
            result = jobj_detector->Smear(*dest_particle);
        }

        if(result == ej::smear::SmearingResults::NotGoneThrough) {
            stat.null_particles++;
            continue;
        }

        if (dest_particle->smear.has_e && dest_particle->smear.has_p) {
            stat.smear_e_smear_p++;
        } else if (dest_particle->smear.has_e && !dest_particle->smear.has_p) {
            stat.smear_e_zero_p++;
        } else if (!dest_particle->smear.has_e && dest_particle->smear.has_p) {
            stat.zero_e_smear_p++;
        } else {
            stat.zero_e_zero_p++;
        }

        if (_verbose>=2) {
            fmt::print("pdg {:<7}  P in {:<10} {:<10}  E in {:<10}  {:<10}\n",
                       source_particle->pdg,
                       source_particle->p, dest_particle->p,
                       source_particle->tot_e, dest_particle->tot_e);
        }

        // Set new full momentum parameter
        dest_particle->p = sqrt(dest_particle->px*dest_particle->px +
                                  dest_particle->py*dest_particle->py +
                                  dest_particle->pz*dest_particle->pz);

        // Adding a particle to the result list
        if(!_throw_not_smeared) {
            dest_particles.push_back(dest_particle.release());
        } else {
            if(_strict_particle_selection && dest_particle->smear.has_p && dest_particle->smear.has_e) {
                dest_particles.push_back(dest_particle.release());
            }
            if(!_strict_particle_selection && dest_particle->smear.has_any_eppid) {
                dest_particles.push_back(dest_particle.release());
            }
        }
    }

    Set(std::move(dest_particles));
}

void SmearingFactory::PrintSmearStats() {
    fmt::print("SmearingFactory statistics:\n");
    auto stat = GetStatistics();
    fmt::print("   total_particles = {:<10} \n", stat.total_particles);
    fmt::print("   null_particles  = {:<10} (not smeared)\n", stat.null_particles);
    fmt::print("   zero_e_zero_p   = {:<10} (not smeared)\n", stat.zero_e_zero_p);
    fmt::print("   zero_e_smear_p  = {:<10} (partly smeared)\n", stat.zero_e_smear_p);
    fmt::print("   smear_e_zero_p  = {:<10} (partly smeared)\n", stat.smear_e_zero_p);
    fmt::print("   smear_e_smear_p = {:<10} (smeared)\n", stat.smear_e_smear_p);
}
