
# ================= G E N F I T =================================
print_header(">>>>    G E N F I T    <<<<" )

if( EXISTS "$ENV{GENFIT_DIR}" )
	message(STATUS "ejana: GENFIT provided by ENV(GENFIT_DIR) $ENV{GENFIT_DIR}")
	set(GENFIT_DIR $ENV{GENFIT_DIR})
elseif(EXISTS ${GENFIT_DIR})
	message(STATUS "ejana: cmake GENFIT provided as ${GENFIT_DIR}")
else()
	message(STATUS "ejana warning: No cmake GENFIT_DIR nor env:GENFIT_DIR provided")
	message(STATUS "ejana warning: Build will use GenfitConfig.cmake or FindGenfit.cmake (if finds them)")
endif()

set(GENFIT_INCLUDE_DIR ${GENFIT_DIR}/include)
find_library(GENFIT_LIBRARY genfit2 HINTS ${GENFIT_DIR}/lib ${GENFIT_DIR}/lib64)
message(STATUS "ejana: GENFIT_LIBRARY      : ${GENFIT_LIBRARY}")
message(STATUS "ejana: GENFIT_INCLUDE_DIR  : ${GENFIT_INCLUDE_DIR}")

find_library(CLHEP CLHEP HITNTS $ENV{CLHEP_LIB_DIR})

# either, the environment variable RAVEPATH has to be specified, and RAVE's RaveConfig.cmake will be used to determine everything we need
# or, Rave_LDFLAGS, Rave_INCLUDE_DIRS and Rave_CFLAGS have to be set via the command-line
#
#
#IF(DEFINED ENV{RAVEPATH})
#    MESSAGE(STATUS "Environment variable RAVEPATH is set. Try to build GFRave.")
#    MESSAGE(STATUS $ENV{RAVEPATH})
#    # get compiler flags from rave
#    INCLUDE(FindPkgConfig)
#    SET(ENV{PKG_CONFIG_PATH} $ENV{RAVEPATH})
#    find_package(Rave REQUIRED)
#
#    SET(RAVE True)
#ELSEIF(DEFINED Rave_LDFLAGS)
#    IF(DEFINED Rave_INCLUDE_DIRS)
#        IF(DEFINED Rave_CFLAGS)
#            MESSAGE(STATUS "Rave_LDFLAGS, Rave_INCLUDE_DIRS and Rave_CFLAGS are set. Try to build GFRave.")
#            SET(RAVE True)
#        ENDIF()
#    ENDIF()
#ELSE()
#    MESSAGE(STATUS "No RAVEPATH or Rave flags given. GFRave will not be built.")
#ENDIF()
#
#IF(DEFINED RAVE)
#    # turn comma separated lists into space separated strings
#    string (REPLACE ";" " " Rave_LDFLAGS_STR "${Rave_LDFLAGS}")
#    string (REPLACE ";" " " Rave_INCLUDE_DIRS_STR "${Rave_INCLUDE_DIRS}")
#    string (REPLACE ";" " " Rave_CFLAGS_STR "${Rave_CFLAGS}")
#
#    SET (CMAKE_CXX_FLAGS ${Rave_DEFINITIONS} ${Rave_CFLAGS_STR})
#    SET(GF_INC_DIRS
#            ./GFRave/include/ ${Rave_INCLUDE_DIRS_STR}
#            )
#
#    AUX_SOURCE_DIRECTORY( ./GFRave/src  library_sources )
#ENDIF()

find_package(Eigen3 REQUIRED)


get_filename_component(PLUGIN_NAME ${CMAKE_CURRENT_LIST_DIR} NAME)

print_header(">>>>   P L U G I N :   ${PLUGIN_NAME}    <<<<")       # Fancy printing
add_plugin(${PLUGIN_NAME})                                          # Creates plugin 'target'

target_sources_local(${PLUGIN_NAME}
	PRIVATE
		trk_fit_genfit.cc
		RecoTrackFactory_Genfit.cc
		RecoTrackFactory_Genfit.h
		EicGenfitWrapper.cc
		EicGenfitWrapper.h
		EventDisplayProcessor.h
		../trk_fit/GeometryService.cc      # TODO: Improve
		../trk_fit/MagneticFieldService.cc # TODO: Improve
		)

include_directories(
	BEFORE
		src/include
	SYSTEM
		src/external/fmt/include
		${JANA_INCLUDE_DIR}
		${EIGEN3_INCLUDE_DIR}
		${ROOT_INCLUDE_DIRS}
		${GENFIT_INCLUDE_DIR})

target_link_libraries(${PLUGIN_NAME}
		${GENFIT_LIBRARY}
		${ROOT_LIBRARIES}
		${ROOT_Eve_LIBRARY}
		${ROOT_Geom_LIBRARY}
		-L${JANA_LIB_DIR}
		JANA
		ejana_fmt)

install(DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/" # source directory
		DESTINATION "include/ejana/plugins/reco/${PLUGIN_NAME}" # target directory
		FILES_MATCHING PATTERN "*.h" # select header files
		)