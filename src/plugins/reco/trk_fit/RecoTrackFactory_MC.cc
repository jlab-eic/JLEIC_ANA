
#include "RecoTrackFactory_MC.h"

#include <JANA/JEvent.h>
#include <MinimalistModel/McTrack.h>
#include <TDatabasePDG.h>

/// RecoTrackFactory_MC simply converts McTracks to RecoTracks.
/// This lets us use 'truth' values when testing vertex fitting, etc.

void RecoTrackFactory_MC::Init() {
    // auto app = GetApplication();
    
    /// Acquire any parameters
    // app->GetParameter("parameter_name", m_destination);
    
    /// Acquire any services
    // m_service = app->GetService<ServiceT>();
    
    /// Set any factory flags
    // SetFactoryFlag(JFactory_Flags_t::NOT_OBJECT_OWNER);
}

void RecoTrackFactory_MC::ChangeRun(const std::shared_ptr<const JEvent> &event) {
    /// This is automatically run before Process, when a new run number is seen
    /// Usually we update our calibration constants by asking a JService
    /// to give us the latest data for this run number
    
    auto run_nr = event->GetRunNumber();
    // m_calibration = m_service->GetCalibrationsForRun(run_nr);
}

void RecoTrackFactory_MC::Process(const std::shared_ptr<const JEvent> &event) {

    /// JFactories are local to a thread, so we are free to access and modify
    /// member variables here. However, be aware that events are _scattered_ to
    /// different JFactory instances, not _broadcast_: this means that JFactory 
    /// instances only see _some_ of the events. 
    
    /// Acquire inputs (This may recursively call other JFactories)
    auto mctracks = event->Get<minimodel::McTrack>();

    std::vector<RecoTrack*> results;

    for (auto* mctrack : mctracks) {
        auto* recotrack = new RecoTrack();
        recotrack->charge = TDatabasePDG::Instance()->GetParticle(mctrack->pdg)->Charge();
        recotrack->track_id = mctrack->id;
        recotrack->x.SetXYZ(mctrack->vtx_x, mctrack->vtx_y, mctrack->vtx_z);
        recotrack->p.SetXYZ(mctrack->p * mctrack->dir_x, mctrack->p * mctrack->dir_y, mctrack->p * mctrack->dir_z);
        // TODO: Populate recotrack->phi and recotrack->theta?
        results.push_back(recotrack);
    }
    Set(results);
}
