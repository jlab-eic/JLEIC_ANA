#ifndef EJANA_CEEMCALCLUSTER_H
#define EJANA_CEEMCALCLUSTER_H

#include <JANA/JObject.h>


struct CeEmcalClusterCell: public JObject {

    uint64_t cluster_id;
    uint64_t address;
    uint64_t cell_global_id;
    uint64_t cell_fraction = 0;
    uint64_t cell_total_energy = 0;
    uint64_t cell_total_adc = 0;
};

//
struct CeEmcalCluster: public JObject {
    explicit CeEmcalCluster(){}

    uint64_t id;
    uint64_t section_id;    // 0 - inner (crystal), 1 - outer (glass)
    double energy;    /// In uncalibrated units

    double abs_x;
    double abs_y;
    double rel_x;      /// Coordinate of incidence is given in length between module centers
    double rel_y;

    double chi2;       /// Chi2
    uint64_t cluster_size;
    uint64_t type;

    std::vector<CeEmcalClusterCell> cells;
};

#endif //EJANA_CEEMCALCLUSTER_H
