
```bash
-Pplugins=trk_eff,trk_fit,trk_fit_genfit,trk_fit_acts,g4e_reader
/Users/nbrei/data/tracking-1-april/minbias_herwig_18x275.root
-PROOT:StartGui=1
-Pnthreads=1
-Pnskip=0
-Ptrk_fit_genfit:interactive=1
-Ptrk_fit_genfit:geometry=/Users/nbrei/data/tracking-1-april/minbias_herwig_18x275.geo.root
-Ptrk_eff:verbose=2
-Ptrk_eff:fitter=acts
```