

# islreco

## Python

Look at [pyjano_example.py](pyjano_example.py) to see the code.

**pyjano** automatically setups everything and re/builds a plugin when the code is changed. 


## Manual build and run from command line:

1. Environment

```bash
export ISLRECO_PROFILE=#<path to profile>
```
2. Jana searches compiled plugins using JANA_PLUGIN_PATH environment variable or in the current directory. 
Make sure you are running ejana where resulting .so file is located or set JANA_PLUGIN_PATH

3. Run 
```bash
ejana 
-Pplugins=islreco,g4e_reader,islreco_test
-Pislreco:verbose=2
-Pislreco:center_profile=profile1_path
-Pislreco:outer_profile=profile2_path
-Pislreco:adc_cut=0.1
-Pjana:DEBUG_PLUGIN_LOADING=1
<source >.root


ejana 
-Pplugins=islreco,g4e_reader,islreco_test
-Pislreco:verbose=2
-Pislreco:center_profile=/home/romanov/eic/islreco/data/prof_lg.dat
-Pislreco:outer_profile=/home/romanov/eic/islreco/data/prof_pwo.dat
-Pislreco:adc_cut=10
-Pjana:DEBUG_PLUGIN_LOADING=1
/home/romanov/eic/calorimetry_studies/work/close_pgun_gamma_4GeV_100.root
```
