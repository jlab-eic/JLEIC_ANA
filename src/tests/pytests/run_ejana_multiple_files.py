from g4epy import Geant4Eic
from pyjano.jana import Jana, PluginFromSource

def run_analysis(file_name):
    jana = Jana(nevents=50000, output=f'{file_name}.ana.root').source(file_name+".root")

    # G4E reader here
    jana.plugin('g4e_reader') \
        .plugin('trk_fit_genfit') \
        .plugin('trk_fit')

    # Builds plots about the track efficiency
    jana.plugin('trk_eff',
                fitter='acts_primary_vertex_fitter')

    # Acts with genfit input
    jana.plugin('trk_fit_acts', input='genfit')

    jana.run()


if __name__ == "__main__":
    particle_counts = [10] # 5
    energies = [1, 2, 3, 5, 7, 10, 15, 20]
    particle_types = ['e-'] #, 'proton']
    num_events = 500
    vtx_x, vtx_y, vtx_z = (0, 0, 0)

    files_names = []
    for particle_count in particle_counts:
        for energy in energies:
            for particle_type in particle_types:
                file_name = f'pgun_{particle_count}prt_{energy}GeV_{particle_type}_{num_events}evt_pos-{vtx_x}-{vtx_y}-{vtx_z}'
                #files_names.append(file_name)
                run_analysis(file_name)

