from g4epy import Geant4Eic


def run_simulation(num_particles, energy, particle_type, num_events, vtx_x=0, vtx_y=0, vtx_z=0):

    g4e = Geant4Eic(beamline='ip6')

    file_name = f'pgun_{num_particles}prt_{energy}GeV_{particle_type}_{num_events}evt_pos-{vtx_x}-{vtx_y}-{vtx_z}'

    # Select cone particle gun generator
    g4e.command('/generator/select coneParticleGun')

    # Set particle to be generated.
    # (geantino is default)
    # (ion can be specified for shooting ions)
    # Common names: proton, e- e+ pi+ pi-
    g4e.command(f'/generator/coneParticleGun/particle {particle_type}')

    # if particle is ion
    # Set properties of ion to be generated.
    # [usage] /gun/ion Z A [Q E flb]
    #         Z:(int) AtomicNumber
    #         A:(int) AtomicMass
    #         Q:(int) Charge of Ion (in unit of e)
    #         E:(double) Excitation energy (in keV)
    #         flb:(char) Floating level base
    # g4e.command('/generator/coneParticleGun/ion Z A [Q E flb]')

    # Set momentum direction
    # Direction needs not to be a unit vector
    g4e.command('/generator/coneParticleGun/direction 0 0 -1')

    # Set kinetic energy [GeV]
    g4e.command(f'/generator/coneParticleGun/energy {energy} GeV')

    # Energy spread [GeV],
    # energy is smeared as gauss (mean=<particle E>, stddev=energyStdDev)
    g4e.command('/generator/coneParticleGun/energyStdDev 0.5 GeV')

    # Cone angle standard deviation.
    # Basically is the resulting cone angle
    g4e.command('/generator/coneParticleGun/coneAngleStdDev 1 rad')

    # Set number of particles to be generated per event
    g4e.command(f'/generator/coneParticleGun/number {num_particles}')
    g4e.command(f'/generator/coneParticleGun/position {vtx_x} {vtx_y} {vtx_z}')

    # To control how many generation of secondaries (tracks and their hits) to save,
    g4e.command(['/rootOutput/saveSecondaryLevel 0'])

    # Extension is omitted here
    # g4e creates a bunch of files with this name and different extensions
    g4e.output(file_name)

    # Run
    g4e.beam_on(num_events).run()


if __name__ == "__main__":
    particle_counts = [5, 10]
    energies = [1, 2, 3, 5, 7, 10, 15, 20]
    particle_types = ['e-', 'proton']
    num_events = 500

    for particle_count in particle_counts:
        for energy in energies:
            for particle_type in particle_types:
                run_simulation(particle_count, energy, particle_type, num_events)
